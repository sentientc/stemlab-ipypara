 # README #

### What is this repository for? ###

  * Quick summary


    A repository for php codes that requires direct editing on mpipom website. automatically generated codes are excluded


  * Version

    Using date of the last commit

  * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

    Yes markdown should make this prettier


  * Set up certificate to login into the controller without password(optional)

    {engine ip}  
```
#!bash
ssh-keygen -t rsa
ssh-copy-id -p {controller ip with account}
```

  * note: 

    i. currently planning on using the account notebook to operate the cluster.  
    ii.it controller(server) may need to login to engine without password(still checking).


### Steps to add ipython engines(calculation nodes) to ipython controller (will switch to passing messages with SSH in the future) ###


## Setup and start ipython engine ##

  notebook@{server ip}(controller and client; pdsh is a remote code execution tool)
  
```
#!bash
ipcluster start --profile='para1'
pdsh -w {engine ip} scp -P {port} {username}@{server ip}:{controller path}ipcontroller-{controll id}-engine.json {engine path}
#each execute register an engine to the controller
pdsh -w {engine ip} ipengine --profile='profile1' --file={engine path}ipcontroller-{controller id}-engine.json
```

### Access ipython with jupyter notebook on controller(steps pending) ###
  * use ipython as client to connect to the controller

### Example python codes ###
  see [full source](https://ipyparallel.readthedocs.io/en/latest/demos.html#million-digits-of-pi)
  
```
#!python
import ipyparallel as ipp
c = ipp.Client(profile='mycluster')
v = c[:]
c.ids
filestring = 'pi200m.ascii.%(i)02dof20'
files = [filestring % {'i':i} for i in range(1,16)]
#direct view
v.map(fetch_pi_file, files)
freqs_all = v.map(compute_two_digit_freqs, files)
```

### to-do ###

  * have to open firewall on local machine to make this functional (currently engines can register but cannot make calculation possible because of firewall)
  * make a working version
  * make a automatic framework to start engines and controller and script to do it
  * message passing with SSH (just need to open port of ssh)
  * ....
